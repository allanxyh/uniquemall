<?php
// Heading
$_['heading_title']      = 'UCash + c c';

// Text
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified UCash + c c payment module!';

// Entry
$_['entry_order_status'] = 'Order Status:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort Order:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment UCash + c c!';
?>