<?php
class Supplier {
	private $supplier_id;
	private $sid;
	private $firstname;
	private $lastname;
	private $email;
	private $telephone;
	private $fax;
	private $address_id;
	private $company;
	private $address;
	private $url;
	
	
  	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');
				
		if (isset($this->session->data['supplier_id'])) { 
			$supplier_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "suppliers WHERE supplier_id = '" . (int)$this->session->data['supplier_id'] . "' AND status = '1'");
			
			if ($supplier_query->num_rows) {
				$this->supplier_id = $supplier_query->row['supplier_id'];
				$this->sid = $supplier_query->row['sid'];
				$this->firstname = $supplier_query->row['firstname'];
				$this->lastname = $supplier_query->row['lastname'];
				$this->email = $supplier_query->row['email'];
				$this->telephone = $supplier_query->row['telephone'];
				$this->fax = $supplier_query->row['fax'];
				
				$this->company = $supplier_query->row['company'];
				$this->address = $supplier_query->row['address'];
				$this->url = $supplier_query->row['url'];
				$this->address_id = $supplier_query->row['address_id'];
				
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "supplier_ip WHERE supplier_id = '" . (int)$this->session->data['supplier_id'] . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");
				
				if (!$query->num_rows) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "supplier_ip SET supplier_id = '" . (int)$this->session->data['supplier_id'] . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', date_added = NOW()");
				}
			} else {
				$this->logout();
			}
  		}
	}
		
public function login($email, $password, $override = false) {
    	//$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($email) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
		//$supplier_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "suppliers WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND password = '" . $this->db->escape(($password)) . "' AND status = '1' AND approved = '1'");
    	
		if ($override) {
			$supplier_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "suppliers where LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND status = '1'");
		} else {
			$supplier_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "suppliers WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
		}
    	
    	if ($supplier_query->num_rows) {
			$this->session->data['supplier_id'] = $supplier_query->row['supplier_id'];
			
			$this->supplier_id = $supplier_query->row['supplier_id'];
			
			$this->sid = $supplier_query->row['sid'];
			$this->firstname = $supplier_query->row['firstname'];
			$this->lastname = $supplier_query->row['lastname'];
			$this->email = $supplier_query->row['email'];
			$this->telephone = $supplier_query->row['telephone'];
			$this->fax = $supplier_query->row['fax'];
			$this->address_id = $supplier_query->row['address_id'];
			
			$this->company = $supplier_query->row['company'];
			$this->address = $supplier_query->row['address'];
			$this->url = $supplier_query->row['url'];
			
			
			$this->db->query("UPDATE " . DB_PREFIX . "suppliers SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE supplier_id = '" . (int)$this->supplier_id . "'");
			
      		return true;
    	} else {
      		return false;
    	}
  	}
  	
	public function logout() {
		
		unset($this->session->data['supplier_id']);

		$this->supplier_id = '';
		$this->sid = '';
		$this->firstname = '';
		$this->lastname = '';
		$this->email = '';
		$this->telephone = '';
		$this->fax = '';
		$this->newsletter = '';
		$this->supplier_group_id = '';
		$this->address_id = '';
		
		session_destroy();
  	}
  
  	public function isLogged() {
    	return $this->supplier_id;
  	}

  	public function getId() {
    	return $this->supplier_id;
  	}
      
  	public function getSid() {
  		return $this->sid;
  	}
  	
  	public function getFirstName() {
		return $this->firstname;
  	}
  
  	public function getLastName() {
		return $this->lastname;
  	}
  
  	public function getEmail() {
		return $this->email;
  	}
  
  	public function getTelephone() {
		return $this->telephone;
  	}
  
  	public function getFax() {
		return $this->fax;
  	}
	
  
	
  	public function getAddressId() {
		return $this->address_id;	
  	}
	
}
?>