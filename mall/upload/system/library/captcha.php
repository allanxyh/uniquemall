<?php
class Captcha {
	protected $code;
	protected $width = 80;
	protected $height = 20;

	function __construct() { 
		$this->code = substr(sha1(mt_rand()), 17, 6); 
	}

	function getCode(){
// 		$out = ob_get_contents();
// 		$out = str_replace(array("\n", "\r", "\t", " "), "", $input);
// 		ob_end_clean();
	  	return $this->code;
	}

	function showImage() {
		Header("Content-type: image/PNG");  
		$im = imagecreate($this->width, $this->height);
		$b = imagecolorallocate($im, 0,0,0); 
		$w = imagecolorallocate($im, 255,255,255); 
		$g = imagecolorallocate($im, 200,200,200); 
		imagefill($im,0,0,$g); 
		$authnum = $this->code;
		imagestring($im, 5, 10, 3, $authnum, $b);
		for($i=0; $i<200; $i++) { 
     		$randcolor = imagecolorallocate($im,rand(0,255),rand(0,255),rand(0,255));
     		imagesetpixel($im, rand()%70 , rand()%30 , $randcolor); 
		}
		$a = imagepng($im); 
		imagedestroy($im);
	}
}
?>