<?php

// Heading
$_['heading_title'] = '您的订单提交失败！';

// Text
$_['text_customer'] = '<p>您的订单提交失败！</p><p>您的余额不足，请查看<a href="%s">我的帐户</a>';
$_['text_guest']    = '<p>您的订单提交失败！</p><p>如果您有任何问题，请联系 <a href="%s">店主</a>。</p>';
$_['text_basket']   = '购物车';
$_['text_checkout'] = '结账';
$_['text_failer']  = '失败';
?>