<?php
class ModelSupplierSupplier extends Model {
	/**
	 * get supplier by id
	 * @param int $supplier_id
	 */
	public function getSupplier($supplier_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "suppliers WHERE supplier_id = '" . (int)$supplier_id . "'");
	
		return $query->row;
	}
	/**
	 * get suppliers counts by email
	 * @param int $email
	 */
	public function getTotalSuppliersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "suppliers WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		
		return $query->row['total'];
	}
	/**
	 * edit supplier
	 * @param array $data
	 */
	public function editSupplier($data) {
		$this->db->query("UPDATE " . DB_PREFIX . "suppliers SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', company = '" . $this->db->escape($data['company']) . "', address = '" . $this->db->escape($data['address']) . "', url = '" . $this->db->escape($data['url']) . "', fax = '" . $this->db->escape($data['fax']) . "' WHERE supplier_id = '" . (int)$this->supplier->getId() . "'");
	}
	
	public function getSupplierEmailBySid($sid){
		$query = $this->db->query("SELECT s.email FROM " . DB_PREFIX . "suppliers s WHERE s.sid = '" . (int)$sid . "'");
		
		return $query->row;
	}
	
	public function getSupplierAddress($supplier){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "supplier_address s WHERE s.supplier_id = '" . (int)$supplier . "'");
		
		return $query->row;
		
	}
	
	public function getAddress($address_id) {
		$address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "supplier_address WHERE address_id = '" . (int)$address_id . "'");
	
		if ($address_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");
	
			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}
	
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");
	
			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}
	
			return array(
					'address_id'     => $address_query->row['address_id'],
					'supplier_id'    => $address_query->row['supplier_id'],
					'firstname'      => $address_query->row['firstname'],
					'lastname'       => $address_query->row['lastname'],
					'company'        => $address_query->row['company'],
					'company_url'    => $address_query->row['company_url'],
					'company_id'     => $address_query->row['company_id'],
					'tax_id'         => $address_query->row['tax_id'],
					'address_1'      => $address_query->row['address_1'],
					'address_2'      => $address_query->row['address_2'],
					'postcode'       => $address_query->row['postcode'],
					'city'           => $address_query->row['city'],
					'zone_id'        => $address_query->row['zone_id'],
					'zone'           => $zone,
					'zone_code'      => $zone_code,
					'country_id'     => $address_query->row['country_id'],
					'country'        => $country,
					'iso_code_2'     => $iso_code_2,
					'iso_code_3'     => $iso_code_3,
					'address_format' => $address_format
			);
		}
	}
}
?>