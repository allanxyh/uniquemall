<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/stylesheet.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>

<div id="content">
  <h1><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h2><?php echo $text_your_details; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
          <td><input type="text" name="firstname" value="<?php echo $firstname; ?>" readonly/>
            <?php if ($error_firstname) { ?>
            <span class="error"><?php echo $error_firstname; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
          <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" readonly/>
            <?php if ($error_lastname) { ?>
            <span class="error"><?php echo $error_lastname; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_email; ?></td>
          <td><input type="text" name="email" value="<?php echo $email; ?>" readonly/>
            <?php if ($error_email) { ?>
            <span class="error"><?php echo $error_email; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
          <td><input type="text" name="telephone" value="<?php echo $telephone; ?>" readonly/>
            <?php if ($error_telephone) { ?>
            <span class="error"><?php echo $error_telephone; ?></span>
            <?php } ?></td>
        </tr>
        
        <tr>
          <td><span class="required">*</span> <?php echo $entry_company; ?></td>
          <td><input type="text" name="company" value="<?php echo $company; ?>" readonly/>
            <?php if ($error_company) { ?>
            <span class="error"><?php echo $error_company; ?></span>
            <?php } ?></td>
        </tr>
        
        <tr>
          <td><span class="required">*</span> <?php echo $entry_url; ?></td>
          <td><input type="text" name="company_url" value="<?php echo $company_url; ?>" readonly/>
           <?php  //if ($error_url) { ?>
            <span class="error"><?php //echo $error_url; ?></span>
            <?php //} ?></td>
        </tr>
        
        <tr>
          <td><span class="required">*</span> <?php echo $entry_address; ?></td>
          <td><input type="text" name="address_1" value="<?php echo $address_1; ?>" readonly/>
            <?php if ($error_address_1) { ?>
            <span class="error"><?php echo $error_address_1; ?></span>
            <?php } ?></td>
        </tr>
        
        <tr>
          <td><span class="required">*</span> <?php echo $entry_city; ?></td>
          <td><input type="text" name="city" value="<?php echo $city; ?>" readonly/>
            <?php //if (//$error_address_1) { ?>
            <span class="error"><?php //echo //$error_address_1; ?></span>
            <?php //} ?></td>
        </tr>
        
        <tr>
          <td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
          <td><input type="text" name="postcode" value="<?php echo $postcode; ?>" readonly/>
            <?php //if (//$error_address_1) { ?>
            <span class="error"><?php //echo //$error_address_1; ?></span>
            <?php //} ?></td>
        </tr>
        
        <tr>
          <td><span class="required">*</span> <?php echo $entry_country; ?></td>
          <td><input type="text" name="country" value="<?php echo $country; ?>" readonly/>
            <?php //if (//$error_address_1) { ?>
            <span class="error"><?php //echo //$error_address_1; ?></span>
            <?php //} ?></td>
        </tr>
        
        <tr>
          <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
          <td><input type="text" name="zone" value="<?php echo $zone; ?>" readonly/>
            <?php //if (//$error_address_1) { ?>
            <span class="error"><?php //echo //$error_address_1; ?></span>
            <?php //} ?></td>
        </tr>
        
        <tr>
          <td><?php echo $entry_fax; ?></td>
          <td><input type="text" name="fax" value="<?php echo $fax; ?>" readonly/></td>
        </tr>
      </table>
    </div>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
      <div class="right">
        <!--<input type="submit" value="<?php //echo $button_continue; ?>" class="button" />  -->
      </div>
    </div>
  </form>
</div>