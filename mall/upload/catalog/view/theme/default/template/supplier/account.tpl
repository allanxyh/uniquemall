
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/stylesheet.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>

<h1><?php echo $heading_title; ?></h1>
<h2><?php echo $text_my_account; ?></h2>
<div class="content">
    <ul>
      <li><a href="<?php echo $edit; ?>"><?php echo $basic_information; ?></a></li>
      <li><a href="<?php echo $my_products; ?>"><?php echo $text_my_products; ?></a></li>
      <li><a href="<?php echo $my_orders; ?>"><?php echo $text_my_orders; ?></a></li>
      <li><a href="<?php echo $my_logout; ?>"><?php echo $text_my_logout; ?></a></li>
    </ul>
</div>
