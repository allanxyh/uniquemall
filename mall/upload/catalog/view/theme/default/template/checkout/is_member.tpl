<div class="left">
<span class="required">*</span><?php echo $text_member_id;?><br/>
<input type = "text" name="aid"/><br/>
<span class="required">*</span><?php echo $text_member_password;?><br/>
<input type="password" name="pwd"/><br/>
</div>
<div class="buttons">
  <div class="right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-is-member" class="button" />
  </div>
</div>


<script type="text/javascript">
$('#button-is-member').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/is_member/validate',
		type: 'post',
		data: $('#is-member input[type=\'text\'],#is-member input[type=\'password\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-is-member').attr('disabled', true);
			$('#button-is-member').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},	
		complete: function() {
			$('#button-is-member').attr('disabled', false);
			
			$('.wait').remove();
		},			
		success: function(json) {
			$('.warning, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			} else if (json['error']) {
				if (json['error']['warning']) {
					$('#is-member .checkout-content').prepend('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
				}			
			} else {
				$.ajax({
					url: 'index.php?route=checkout/payment_method',
					dataType: 'html',
					success: function(html) {
						
						$('#payment-method .checkout-content').html(html);
						
						$('#is-member .checkout-content').slideUp('slow');
						
						$('#payment-method .checkout-content').slideDown('slow');

						$('#is-member .checkout-heading a').remove();
						$('#payment-method .checkout-heading a').remove();
						
						$('#is-member .checkout-heading').append('<a><?php echo $text_modify; ?></a>');	
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});					
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});		
		
</script>