<?php
class ControllerCheckoutSuccess extends Controller { 
	public function index() {
		 if(isset($this->session->data['order_id'])){
		 	
		 	$re = "";
		 	$a = $this->session->data['order_id'];
		 	//订单对象 order
		 	$order = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$a . "'");
		 	//获取订单的总金额   order ucash total
		 	$uTotal = $order->row['total'];
		 	//获取花费的消费积分  order cc total
		 	$integral = ($uTotal['value']/3)*2;
		 	$this->data['integral'] = $integral; 
		 			 	
		 	$aid = $this->session->data['member']['aid']; //member id
		 	
		 	//与A平台数据库连接  
		 	 connect A    testDB
		 	$dbconn = pg_connect("host=173.255.245.164 dbname=db0114 user=youadweb password=test111") or die('Query failed: ' . pg_last_error());
		 	
		 	connect A   productionDB
		 	//$dbconn = pg_connect('host=74.207.254.26 dbname=youad user=adit_sz password=$Pge#3Axt1') or die('Query failed: ' . pg_last_error());
		 	 *
		 	//获取有效Ucash
		 	$select_cash="select least(ucash_balance,ucash_balance_available) from distributors where id =".$aid;
		 	$result = pg_query($dbconn, $select_cash) or die('Query failed: ' . pg_last_error());
		 	$cash_value = pg_fetch_row($result);
		 	$ucash = $cash_value[0]; //有效的Ucash
		 	
		 	//得到消费积分
		 	$select_cp = "select least(ushares_balance, ushares_balance_available) from distributors where id =".$aid;
		 	$result = pg_query($dbconn, $select_cp) or die('Query failed: ' . pg_last_error());
		 	$cp_value = pg_fetch_row($result);
		 	$cp = $cp_value[0] ;//是有效的cp值
		 	
		 	if(($ucash>=$uTotal) && ($cp>=$integral)){
		 		//$this->load->model('checkout/order');
		 		//$this->session->data['order_id'] = $this->model_checkout_order->addOrder($data);
		 	
		 		$b = $ucash-$uTotal;//减掉买东西后还有的ucash
		 		//$this->db->query("UPDATE " . DB_PREFIX . "customer SET ucash = '" . (int)$b . "' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($customer->row['email'])) . "'");
		 		//扣减Ucash
		 	
		 		$query = "update distributors set ucash_balance = ucash_balance - " .$uTotal. ",ucash_balance_available = ucash_balance_available - ".$uTotal." where id = ".$aid .";";
		 		$result = pg_query($dbconn, $query) or die('Query failed: ' . pg_last_error());
		 		if($result == false){
		 			$re =  $this->url->link('checkout/failer');
		 			$this->data['redirect'] = $re;
		 		}
		 	
		 		//记录日志
		 		$query = "insert into cash_journal (transaction_type,distributor_id,happen_amount,ucash_before,ucash_after,operator_id)values ('shopping',$aid,$uTotal,$ucash,$b,$aid)";
		 		$result = pg_query($dbconn, $query) or die('Query failed: ' . pg_last_error());
		 		if($result == false){
		 			$re =  $this->url->link('checkout/failer');
		 			$this->data['redirect'] = $re;
		 		}
		 		//扣减消费积分
		 		$update_cp = "update distributors set ushares_balance = ushares_balance -".$integral .",ushares_balance_available = ushares_balance_available - ".$integral." where id =" .$aid ." ;";
		 		$result = pg_query($dbconn, $update_cp) or die('Query failed: ' . pg_last_error());
		 		if($result == false){
		 			$re =  $this->url->link('checkout/failer');
		 			$this->data['redirect'] = $re;
		 		}
		 		$share_after = $cp-$integral;
		 	
		 		//记录log
		 		$insert_log = "insert into trading_log (trading_type,share_id,distributor_id,operator_id,happen_quantity,share_before,share_after) values('en_B_adview','B01',$aid,$aid,$integral,$cp,$share_after)";
		 		$result = pg_query($dbconn, $insert_log) or die('Query failed: ' . pg_last_error());
		 		if($result == false){
		 			$re =  $this->url->link('checkout/failer');
		 			$this->data['redirect'] = $re;
		 		}
		 		pg_free_result($result);
		 		pg_close($dbconn);
		 	
		 		$operation = "已支付";
		 		
		 		
		 		$log = Array(
		 				'order_id'  => $a,
		 				'member_id'	=> $aid,
		 				'operation' => $operation
		 		);
		 		
		 		$this->load->model('log/order_log');
		 		$this->model_log_order_log->addOrderLog($log);
		 	
		 		if (isset($this->session->data['order_id'])) {
		 			$this->cart->clear();
		 	
		 			unset($this->session->data['shipping_method']);
		 			unset($this->session->data['shipping_methods']);
		 			unset($this->session->data['payment_method']);
		 			unset($this->session->data['payment_methods']);
		 			unset($this->session->data['guest']);
		 			unset($this->session->data['comment']);
		 			unset($this->session->data['order_id']);
		 			unset($this->session->data['coupon']);
		 			unset($this->session->data['reward']);
		 			unset($this->session->data['voucher']);
		 			unset($this->session->data['vouchers']);
		 			unset($this->session->data['member']);
		 			unset($this->session->data['member']['aid']);
		 		}
		 	
		 		$this->language->load('checkout/success');
		 	
		 		$this->document->setTitle($this->language->get('heading_title'));
		 	
		 		$this->data['breadcrumbs'] = array();
		 	
		 		$this->data['breadcrumbs'][] = array(
		 				'href'      => $this->url->link('common/home'),
		 				'text'      => $this->language->get('text_home'),
		 				'separator' => false
		 		);
		 	
		 		$this->data['breadcrumbs'][] = array(
		 				'href'      => $this->url->link('checkout/cart'),
		 				'text'      => $this->language->get('text_basket'),
		 				'separator' => $this->language->get('text_separator')
		 		);
		 	
		 		$this->data['breadcrumbs'][] = array(
		 				'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
		 				'text'      => $this->language->get('text_checkout'),
		 				'separator' => $this->language->get('text_separator')
		 		);
		 	
		 		$this->data['breadcrumbs'][] = array(
		 				'href'      => $this->url->link('checkout/success'),
		 				'text'      => $this->language->get('text_success'),
		 				'separator' => $this->language->get('text_separator')
		 		);
		 	
		 		$this->data['heading_title'] = $this->language->get('heading_title');
		 	
		 		if ($this->customer->isLogged()) {
		 			$this->data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', 'SSL'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/download', '', 'SSL'), $this->url->link('information/contact'));
		 		} else {
		 			$this->data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		 		}
		 	
		 		$this->data['button_continue'] = $this->language->get('button_continue');
		 	
		 		$this->data['continue'] = $this->url->link('common/home');
		 	
		 		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
		 			$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
		 		} else {
		 			$this->template = 'default/template/common/success.tpl';
		 		}
		 	
		 		$this->children = array(
		 				'common/column_left',
		 				'common/column_right',
		 				'common/content_top',
		 				'common/content_bottom',
		 				'common/footer',
		 				'common/header'
		 		);
		 	
		 		$this->response->setOutput($this->render());
		 		
		 	
		 	}else{
		 		$re =  $this->url->link('checkout/failer');
		 		$this->data['redirect'] = $re;
		 	} 
		 }else{
		 	$re =  $this->url->link('common/home');
		 	$this->data['redirect'] = $re;
		 	if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
		 		$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
		 	} else {
		 		$this->template = 'default/template/common/success.tpl';
		 	}
		 	
		 	$this->children = array(
		 			'common/column_left',
		 			'common/column_right',
		 			'common/content_top',
		 			'common/content_bottom',
		 			'common/footer',
		 			'common/header'
		 	);
		 	
		 	$this->response->setOutput($this->render());
		 }
  	}
}
?>