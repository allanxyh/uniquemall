<?php
class ControllerCheckoutIsMember extends Controller {
  	public function index() {
		$this->language->load('checkout/checkout');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['text_member_id'] = $this->language->get('text_member_id');
		$this->data['text_member_password'] = $this->language->get('text_member_password');
		
		$this->load->model('account/address');
		
		if ($this->customer->isLogged() && isset($this->session->data['shipping_address_id'])) {					
			$shipping_address = $this->model_account_address->getAddress($this->session->data['shipping_address_id']);		
		} elseif (isset($this->session->data['guest'])) {
			$shipping_address = $this->session->data['guest']['shipping'];
		}
		
		if (!empty($shipping_address)) {
			// Shipping Methods
			$quote_data = array();
			
			$this->load->model('setting/extension');
			
			$results = $this->model_setting_extension->getExtensions('shipping');
			
			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('shipping/' . $result['code']);
					
					$quote = $this->{'model_shipping_' . $result['code']}->getQuote($shipping_address); 
		
					if ($quote) {
						$quote_data[$result['code']] = array( 
							'title'      => $quote['title'],
							'quote'      => $quote['quote'], 
							'sort_order' => $quote['sort_order'],
							'error'      => $quote['error']
						);
					}
				}
			}
	
			$sort_order = array();
		  
			foreach ($quote_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}
	
			array_multisort($sort_order, SORT_ASC, $quote_data);
			
			$this->session->data['shipping_methods'] = $quote_data;
		}
					
		$this->data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$this->data['text_comments'] = $this->language->get('text_comments');
	
		$this->data['button_continue'] = $this->language->get('button_continue');
		
		if (empty($this->session->data['shipping_methods'])) {
			$this->data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
		} else {
			$this->data['error_warning'] = '';
		}	
					
		if (isset($this->session->data['shipping_methods'])) {
			$this->data['shipping_methods'] = $this->session->data['shipping_methods']; 
		} else {
			$this->data['shipping_methods'] = array();
		}
		
		if (isset($this->session->data['shipping_method']['code'])) {
			$this->data['code'] = $this->session->data['shipping_method']['code'];
		} else {
			$this->data['code'] = '';
		}
		
		if (isset($this->session->data['comment'])) {
			$this->data['comment'] = $this->session->data['comment'];
		} else {
			$this->data['comment'] = '';
		}
			
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/is_member.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/checkout/is_member.tpl';
		} else {
			$this->template = 'default/template/checkout/is_member.tpl';
		}
		
		$this->response->setOutput($this->render());
  	}
	
	public function validate() {
		$this->language->load('checkout/checkout');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$json = array();
		$aid = $this->request->post['aid'];
		$pwd = $this->request->post['pwd'];
		
		if(!(preg_match("/^[0-9]*$/",$aid))){
			$json['error']['warning'] = $this->language->get('error_id');// "您的ID只能是数字";
			
		}
		
		if(!$json){
			$this->session->data['member']['aid']=$aid;
			
			
			$dbconn = pg_connect("host=173.255.245.164 port=5432 dbname=db0114 user=youadweb password=test111") or die('Could not connect: ' . pg_last_error());//测试
			//$dbconn = pg_connect('host=74.207.254.26 dbname=youad user=adit_sz password=$Pge#3Axt1') or die('Query failed: ' . pg_last_error());//生产
			
			//得到密码
			$select_cp ="select password from users uu, distributors dd where uu.account_id = dd.account_id and dd.id = ".$aid;
			$result = pg_query($dbconn, $select_cp) or die('Query failed: ' . pg_last_error());
			$row = pg_fetch_row($result);
			$pd = $row[0];
			
			if(($pd!=hash('sha224', $pwd))){  //
				$json['error']['warning'] =  $this->language->get('error_memeber');//"ID or Password error!";
			}
			
			pg_free_result($result);
			pg_close($dbconn);
			
		}
		
 		$this->response->setOutput(json_encode($json));	
	}
}
?>