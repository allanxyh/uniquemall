<?php       
class ControllerSupplierLogout extends Controller {   
	public function index() { 
    	$this->supplier->logout();
 
 		unset($this->session->data['token']);

		$this->redirect($this->url->link('supplier/login', '', 'SSL'));
  	}
}  
?>