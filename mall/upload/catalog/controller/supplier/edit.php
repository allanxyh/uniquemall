<?php
class ControllerSupplierEdit extends Controller {
	private $error = array();

	public function index() {
		if (!$this->supplier->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('supplier/edit', '', 'SSL');

			$this->redirect($this->url->link('supplier/login', '', 'SSL'));
		}

		$this->language->load('supplier/edit');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		
		$this->load->model('supplier/supplier');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_supplier_supplier->editSupplier($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('supplier/account', '', 'SSL'));
		}
		
   
      	
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_your_details'] = $this->language->get('text_your_details');

		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_telephone'] = $this->language->get('entry_telephone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');

		$this->data['entry_company'] = $this->language->get('entry_company');
		$this->data['entry_address'] = $this->language->get('entry_address');
		$this->data['entry_url'] = $this->language->get('entry_url');
		
		
		$this->data['entry_city'] = $this->language->get('entry_city');
		$this->data['entry_postcode'] = $this->language->get('entry_postcode');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zone'] = $this->language->get('entry_zone');
		
		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_back'] = $this->language->get('button_back');
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['firstname'])) {
			$this->data['error_firstname'] = $this->error['firstname'];
		} else {
			$this->data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$this->data['error_lastname'] = $this->error['lastname'];
		} else {
			$this->data['error_lastname'] = '';
		}
		
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}	
		
		if (isset($this->error['telephone'])) {
			$this->data['error_telephone'] = $this->error['telephone'];
		} else {
			$this->data['error_telephone'] = '';
		}	
		//error_company
		if (isset($this->error['company'])) {
			$this->data['error_company'] = $this->error['company'];
		} else {
			$this->data['error_company'] = '';
		}
		//error_address
		if (isset($this->error['address_1'])) {
			$this->data['error_address_1'] = $this->error['address_1'];
		} else {
			$this->data['error_address_1'] = '';
		}
		
		$this->data['action'] = $this->url->link('supplier/edit', '', 'SSL');

		if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			$supplier_info = $this->model_supplier_supplier->getSupplier($this->supplier->getId());
			$supplier_address_info = $this->model_supplier_supplier->getAddress($this->supplier->getId());
		}
		
		if (isset($this->request->post['firstname'])) {
			$this->data['firstname'] = $this->request->post['firstname'];
		} elseif (isset($supplier_info)) {
			$this->data['firstname'] = $supplier_info['firstname'];
		} else {
			$this->data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$this->data['lastname'] = $this->request->post['lastname'];
		} elseif (isset($supplier_info)) {
			$this->data['lastname'] = $supplier_info['lastname'];
		} else {
			$this->data['lastname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (isset($supplier_info)) {
			$this->data['email'] = $supplier_info['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$this->data['telephone'] = $this->request->post['telephone'];
		} elseif (isset($supplier_info)) {
			$this->data['telephone'] = $supplier_info['telephone'];
		} else {
			$this->data['telephone'] = '';
		}

		if (isset($this->request->post['fax'])) {
			$this->data['fax'] = $this->request->post['fax'];
		} elseif (isset($supplier_info)) {
			$this->data['fax'] = $supplier_info['fax'];
		} else {
			$this->data['fax'] = '';
		}
		 
		
		if (isset($this->request->post['company'])) {
			$this->data['company'] = $this->request->post['company'];
		} elseif (isset($supplier_info)) {
			$this->data['company'] = $supplier_address_info['company'];
		} else {
			$this->data['company'] = '';
		}
		
		if (isset($this->request->post['company_url'])) {
			$this->data['company'] = $this->request->post['company_url'];
		} elseif (isset($supplier_address_info)) {
			$this->data['company_url'] = $supplier_address_info['company_url'];
		} else {
			$this->data['company_url'] = '';
		}
		
		if (isset($this->request->post['address_1'])) {
			$this->data['address_1'] = $this->request->post['address_1'];
		} elseif (isset($supplier_address_info)) {
			$this->data['address_1'] = $supplier_address_info['address_1'];
		} else {
			$this->data['address_1'] = '';
		}
		
		if (isset($this->request->post['city'])) {
			$this->data['city'] = $this->request->post['city'];
		} elseif (isset($supplier_address_info)) {
			$this->data['city'] = $supplier_address_info['city'];
		} else {
			$this->data['city'] = '';
		}
		
		if (isset($this->request->post['postcode'])) {
			$this->data['postcode'] = $this->request->post['postcode'];
		} elseif (isset($supplier_address_info)) {
			$this->data['postcode'] = $supplier_address_info['postcode'];
		} else {
			$this->data['postcode'] = '';
		}
		
		if (isset($this->request->post['country'])) {
			$this->data['country'] = $this->request->post['country'];
		} elseif (isset($supplier_address_info)) {
			$this->data['country'] = $supplier_address_info['country'];
		} else {
			$this->data['country'] = '';
		}
		
		if (isset($this->request->post['zone'])) {
			$this->data['zone'] = $this->request->post['zone'];
		} elseif (isset($supplier_address_info)) {
			$this->data['zone'] = $supplier_address_info['zone'];
		} else {
			$this->data['zone'] = '';
		}
		
		$this->data['back'] = $this->url->link('supplier/account', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/supplier/edit.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/supplier/edit.tpl';
		} else {
			$this->template = 'default/template/supplier/edit.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
						
		$this->response->setOutput($this->render());	
	}

	public function validate() {
		if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen($this->request->post['firstname']) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen($this->request->post['lastname']) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}
		
		if (($this->supplier->getEmail() != $this->request->post['email']) && $this->model_supplier_supplier->getTotalSuppliersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}
		//validate company name
		if ((utf8_strlen($this->request->post['company']) < 3) || (utf8_strlen($this->request->post['company']) > 32)) {
			$this->error['company'] = $this->language->get('error_company');
		}
// 		//validate company address
		if ((utf8_strlen($this->request->post['address_1']) < 6) || (utf8_strlen($this->request->post['address_1']) > 120)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>