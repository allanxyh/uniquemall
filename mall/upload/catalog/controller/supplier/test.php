<?php
class ControllerSupplierTest extends Controller {
	public function index(){
		$order_info = $this->getOrder(344);
		$this->template = 'default/template/supplier/test.tpl';
		
		$this->children = array(
				'common/header',
				'common/footer'
		);
		$this->load->model('catalog/product');
		$product_info = $this->model_catalog_product->getProduct(28);
		$product_options = $this->model_catalog_product->getProductOptions(28);
		
		$query = $this->db->query("SELECT s.email FROM " . DB_PREFIX . "suppliers s WHERE s.sid = '" . (int)1002 . "'");
		
		
		$this->response->setOutput($this->render());
		//print_r($order_query->row['supplier']); //$order_info
		print_r($query->row['email']);
		//print_r($this->config->get('free_checkout_order_status_id')); $this->cart->getProducts()
		$products = $this->cart->getProducts();
		foreach ($products as $product){
// 				if($product == ){
		
// 			     }
			//print_r($product['supplier']);
		}
		
	
		//print_r($this->config->get('config_alert_mail'));
		
	}
	
	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");
	
		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");
	
			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}
	
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");
	
			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}
	
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");
	
			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}
	
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");
	
			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}
	
			$this->load->model('localisation/language');
	
			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);
	
			if ($language_info) {
				$language_code = $language_info['code'];
				$language_filename = $language_info['filename'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_filename = '';
				$language_directory = '';
			}
	
			return array(
					'order_id'                => $order_query->row['order_id'],
					'invoice_no'              => $order_query->row['invoice_no'],
					'invoice_prefix'          => $order_query->row['invoice_prefix'],
					'store_id'                => $order_query->row['store_id'],
					'store_name'              => $order_query->row['store_name'],
					'store_url'               => $order_query->row['store_url'],
					'customer_id'             => $order_query->row['customer_id'],
					'firstname'               => $order_query->row['firstname'],
					'lastname'                => $order_query->row['lastname'],
					'telephone'               => $order_query->row['telephone'],
					'fax'                     => $order_query->row['fax'],
					'email'                   => $order_query->row['email'],
					'payment_firstname'       => $order_query->row['payment_firstname'],
					'payment_lastname'        => $order_query->row['payment_lastname'],
					'payment_company'         => $order_query->row['payment_company'],
					'payment_company_id'      => $order_query->row['payment_company_id'],
					'payment_tax_id'          => $order_query->row['payment_tax_id'],
					'payment_address_1'       => $order_query->row['payment_address_1'],
					'payment_address_2'       => $order_query->row['payment_address_2'],
					'payment_postcode'        => $order_query->row['payment_postcode'],
					'payment_city'            => $order_query->row['payment_city'],
					'payment_zone_id'         => $order_query->row['payment_zone_id'],
					'payment_zone'            => $order_query->row['payment_zone'],
					'payment_zone_code'       => $payment_zone_code,
					'payment_country_id'      => $order_query->row['payment_country_id'],
					'payment_country'         => $order_query->row['payment_country'],
					'payment_iso_code_2'      => $payment_iso_code_2,
					'payment_iso_code_3'      => $payment_iso_code_3,
					'payment_address_format'  => $order_query->row['payment_address_format'],
					'payment_method'          => $order_query->row['payment_method'],
					'payment_code'            => $order_query->row['payment_code'],
					'shipping_firstname'      => $order_query->row['shipping_firstname'],
					'shipping_lastname'       => $order_query->row['shipping_lastname'],
					'shipping_company'        => $order_query->row['shipping_company'],
					'shipping_address_1'      => $order_query->row['shipping_address_1'],
					'shipping_address_2'      => $order_query->row['shipping_address_2'],
					'shipping_postcode'       => $order_query->row['shipping_postcode'],
					'shipping_city'           => $order_query->row['shipping_city'],
					'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
					'shipping_zone'           => $order_query->row['shipping_zone'],
					'shipping_zone_code'      => $shipping_zone_code,
					'shipping_country_id'     => $order_query->row['shipping_country_id'],
					'shipping_country'        => $order_query->row['shipping_country'],
					'shipping_iso_code_2'     => $shipping_iso_code_2,
					'shipping_iso_code_3'     => $shipping_iso_code_3,
					'shipping_address_format' => $order_query->row['shipping_address_format'],
					'shipping_method'         => $order_query->row['shipping_method'],
					'shipping_code'           => $order_query->row['shipping_code'],
					'comment'                 => $order_query->row['comment'],
					'total'                   => $order_query->row['total'],
					'order_status_id'         => $order_query->row['order_status_id'],
					'order_status'            => $order_query->row['order_status'],
					'language_id'             => $order_query->row['language_id'],
					'language_code'           => $language_code,
					'language_filename'       => $language_filename,
					'language_directory'      => $language_directory,
					'currency_id'             => $order_query->row['currency_id'],
					'currency_code'           => $order_query->row['currency_code'],
					'currency_value'          => $order_query->row['currency_value'],
					'ip'                      => $order_query->row['ip'],
					'forwarded_ip'            => $order_query->row['forwarded_ip'],
					'user_agent'              => $order_query->row['user_agent'],
					'accept_language'         => $order_query->row['accept_language'],
					'supplier'                => $order_query->row['supplier'],
					'date_modified'           => $order_query->row['date_modified'],
					'date_added'              => $order_query->row['date_added']
			);
		} else {
			return false;
		}
	}
}
?>