<?php 
class ControllerSupplierAccount extends Controller {
	public function index(){
		
		if (!$this->supplier->isLogged()) {// $this->session->data['supplier_id']
			$this->session->data['redirect'] = $this->url->link('supplier/account', '', 'SSL');
		
			$this->redirect($this->url->link('supplier/login', '', 'SSL'));
		}
		
		$this->language->load('supplier/account');
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['basic_information'] = $this->language->get('basic_information');
		$this->data['text_my_account'] = $this->language->get('text_my_account');
		$this->data['text_my_products'] = $this->language->get('text_my_products');
		$this->data['text_my_orders'] = $this->language->get('text_my_orders');
		$this->data['text_my_logout'] = $this->language->get('text_my_logout');
	
		$this->data['edit'] = $this->url->link('supplier/edit', '', 'SSL');
		$this->data['my_products'] = $this->url->link('supplier/product', '', 'SSL');
		$this->data['my_orders'] = $this->url->link('sale/order', '', 'SSL');
		$this->data['my_logout'] = $this->url->link('supplier/logout', '', 'SSL');
		
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		
		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/supplier/account.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/supplier/account.tpl';
		} else {
			$this->template = 'default/template/supplier/account.tpl';
		}
		
		$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
		);
		
		$this->response->setOutput($this->render());
	}
}

?>