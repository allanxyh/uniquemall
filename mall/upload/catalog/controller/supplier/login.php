<?php  
class ControllerSupplierLogin extends Controller { 
	private $error = array();
	          
	public function index() { 
		
    	$this->language->load('common/login');

		$this->document->setTitle($this->language->get('heading_title'));
		
		if ($this->supplier->isLogged() && isset($this->request->get['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			$this->redirect($this->url->link('supplier/account', '', 'SSL'));
		}
		 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->session->data['token'] = md5(mt_rand());
			
			//$this->redirect($this->url->link('supplier/account', 'token=' . $this->session->data['token'], 'SSL'));
			if (isset($this->request->post['redirect'])) {
				$this->redirect($this->request->post['redirect'] . '&token=' . $this->session->data['token']);
			}else{
				$this->redirect($this->url->link('supplier/account', 'token=' . $this->session->data['token'], 'SSL'));
			}
		}
		
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_login'] = $this->language->get('text_login');
		$this->data['text_forgotten'] = $this->language->get('text_forgotten');
		
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_password'] = $this->language->get('entry_password');
		
		$this->data['button_login'] = $this->language->get('button_login');
		 
		if ((isset($this->session->data['token']) && !isset($this->request->get['token'])) || ((isset($this->request->get['token']) && (isset($this->session->data['token']) && ($this->request->get['token'] != $this->session->data['token']))))) {
			$this->error['warning'] = $this->language->get('error_token');
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
    
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
				
    	$this->data['action'] = $this->url->link('supplier/login', '', 'SSL');
    	
		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} else {
			$this->data['email'] = '';
		}
		
		if (isset($this->request->post['password'])) {
			$this->data['password'] = $this->request->post['password'];
		} else {
			$this->data['password'] = '';
		}
		
		if (isset($this->request->get['route'])) {
			$route = $this->request->get['route'];
			
			unset($this->request->get['route']);
			
			if (isset($this->request->get['token'])) {
				unset($this->request->get['token']);
			}
			
			$url = '';
						
			if ($this->request->get) {
				$url .= http_build_query($this->request->get);
			}
		
			$this->data['redirect'] = $this->url->link($route, $url, 'SSL');
			
		} else {
			$this->data['redirect'] = '';	
		}
		 
		$this->data['action'] = $this->url->link('supplier/login', '', 'SSL');
		$this->template = 'default/template/supplier/login.tpl';
		
		$this->children = array(
			'common/header',
			'common/footer'
		); 
				
		$this->response->setOutput($this->render());
  	}
		
	public function validate() {
		
		if (isset($this->request->post['email']) && isset($this->request->post['password']) && !$this->supplier->login($this->request->post['email'], $this->request->post['password'])) {
			$this->error['warning'] = $this->language->get('error_login');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}  
?>